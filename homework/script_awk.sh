#!/bin/bash
df -h > filesys_usage.txt
mpstat > cpu_stat.txt
echo "disk usage:"
  awk 'NR > 1 {print "Filesystem:", $1, "- Size:", $2, "- Used:", $3, "- Available:", $4, "- Use%:", $5, "- Mounted on:", $6}' filesys_usage.txt 
echo "cpu usage:"
awk 'NR > 3 {print "CPU: ", $3, " user use: ", $4, " system use: ", $6, " idle: " $13}' cpu_stat.txt
